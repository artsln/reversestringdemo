﻿using FluentAssertions;
using Utils.StringUtils;
using Xunit;

namespace Tests.StringUtilsTests
{
    public class ReverseStringUtilTests
    {
        [Theory]
        [InlineData("the sky is blue", "blue is sky the")]
        [InlineData("blue is sky the", "the sky is blue")]
        [InlineData("number 111 222", "222 111 number")]
        [InlineData("test", "test")]
        [InlineData(" test string ", "string test")]
        public void ReverseStringTest(string text, string expectedReversedText)
        {
            // arrange & act
            var result = ReverseStringUtil.Reverse(text);

            // assert
            result.Should().Be(expectedReversedText);
        }
    }
}