﻿using System.Linq;
using static System.String;

namespace Utils.StringUtils
{
    public static class ReverseStringUtil
    {
        public static string Reverse(string text)
        {
            var textAfterTrim = text.Trim();
            var textsAfterSplit = textAfterTrim.Split(" ");
            var textsAfterReverse = textsAfterSplit.Reverse();
            var result = Join(" ", textsAfterReverse.ToArray());

            return result;
        }
    }
}